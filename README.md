# Spring Data JPA with REST
The purpose of this example is to demonstrate the functionality of [HATEOAS][HATEOUS] and [HAL Browser][HAL] using
[Spring Data][Data] [JPA][JPA].

## Running the sample
You have two options to run the sample

1.  `./gradlew bootRun`: run with `gradle`
2.  `mvn spring-boot:run`: run with `maven`

Once the server starts on `8080` you can use any REST client to perform operations.
You can test with:
`curl http://localhost:8080`

sample response is:
```json
{
  "_links" : {
    "people" : {
      "href" : "http://localhost:8080/people{?page,size,sort}",
      "templated" : true
    },
    "profile" : {
      "href" : "http://localhost:8080/profile"
    }
  }
}
```
Here you get a first glimpse of what this server has to offer. There is a people link located at http://localhost:8080/people. It has some options such as `?page`, `?size`, and `?sort`.
>  	**NOTE**: Spring Data REST uses the HAL format for JSON output. It is flexible and offers a convenient way to supply links adjacent to the data that is served

`curl http://localhost:8080/people`
```json
{
  "_embedded" : {
    "people" : [ ]
  },
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/people"
    },
    "profile" : {
      "href" : "http://localhost:8080/profile/people"
    },
    "search" : {
      "href" : "http://localhost:8080/people/search"
    }
  },
  "page" : {
    "size" : 20,
    "totalElements" : 0,
    "totalPages" : 0,
    "number" : 0
  }
}
```

There are currently no elements and hence no pages. Time to create a new `Person`!
`curl -i -X POST -H "Content-Type:application/json" -d "{ \"lastName\": \"Krah\", \"firstName\": \"Julius\" }" http://localhost:8080/people`

```json
{
  "firstName" : "Julius",
  "lastName" : "Krah",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/people/1"
    },
    "person" : {
      "href" : "http://localhost:8080/people/1"
    }
  }
}
```
You can delete records:
`curl -X DELETE http://localhost:8080/people/1`


[HATEOUS]: https://spring.io/understanding/HATEOAS
[JPA]: http://www.tutorialspoint.com/jpa/
[HAL]: http://stateless.co/hal_specification.html
[Data]: http://projects.spring.io/spring-data-jpa/
